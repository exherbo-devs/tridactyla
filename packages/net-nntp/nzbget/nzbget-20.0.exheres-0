# Copyright 2009, 2012 Michael Forney
# Distributed under the terms of the GNU General Public License v2

require option-renames [ renames=[ 'gnutls providers:gnutls' ] ]
# github [ suffix="tar.gz" release="v20.0" ]

SUMMARY="NZBGet is a command-line based binary newsgrabber for nzb files"
DESCRIPTION="
NZBGet is a command-line based binary newsgrabber for nzb files, written in C++. It supports
client/server mode, automatic par-check/-repair and web-interface (via additional package). NZBGet
requires low system resources and runs great on routers, NAS-devices and media players.
"
HOMEPAGE="http://nzbget.net/"
DOWNLOADS="https://github.com/${PN}/${PN}/releases/download/v${PV}/${PNV}-src.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    par2 [[ description = [ Support for checking files with integrated par2-module (libpar2) ] ]]

    ( providers: gnutls libressl openssl ) [[ number-selected = at-most-one ]]
"
# Since version 14.x, nzbget is bundled with a patched version of libpar2 instead of using the
# external lib. Thus, we're using the bundled version for now.

DEPENDENCIES="
    build+run:
        dev-libs/libxml2
        sys-libs/ncurses

        providers:gnutls? ( dev-libs/gnutls )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( nzbget.conf.example )

src_configure() {
    local tlslib
    if option providers:gnutls ; then
        tlslib=--with-tlslib=GnuTLS
    elif option providers:libressl || option providers:openssl ; then
        tlslib=--with-tlslib=OpenSSL
    else
        tlslib=--disable-tls
    fi

    econf \
        --enable-curses \
        $(option_enable par2 parcheck) \
        ${tlslib}
}

